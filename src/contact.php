<?php
define("WEBMASTER_EMAIL", 'mk@eagletyres.com.au'); // Enter yor e-mail
 
error_reporting (E_ALL); 

if(!empty($_POST)){
	$_POST = array_map('trim', $_POST); 
	$name = htmlspecialchars($_POST['name']);
	$email = $_POST['email'];
	$subject = htmlspecialchars($_POST['subject']);
	$message = htmlspecialchars($_POST['message']);
	 
	$error = array();
	 
	 
	if(empty($name)){
		$error[] = 'Please enter your name';
	}
	 
	 
	if(empty($email)){
		$error[] = 'Please enter your e-mail';
	}elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){ 
		$error[] = 'e-mail is incorrect';
	}
	 
	 
	if(empty($message) || empty($message{15})){
		$error[] = "Please enter a valid message";
	}
	 
	if(empty($error)){ 
		$message = 'Name: ' . $name . '
		Email: ' . $email . '
		Subject: ' . $subject . '
		Message: ' . $message;
		$mail = mail(WEBMASTER_EMAIL, 'Message from the Marcello Wheels website', $message,
			 "From: ".$name." \r\n"
			."Reply-To: ".$email."\r\n"
			."X-Mailer: PHP/" . phpversion());
		 
		if($mail){
			echo 'OK';
		}
	}
	else{
		echo '<div class="notification_error">'.implode('<br />', $error).'</div>';
	}
}